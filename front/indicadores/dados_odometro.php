<?php     

      $ano = date("Y");
      $month = date("Y-m");
      $hoje = date("Y-m-d");

      // Número de chamados no ano atual.
      $sql_ano =	
            "SELECT 
                  COUNT(glpi_tickets.id) AS total        
            FROM 
                  glpi_tickets
                  LEFT JOIN glpi_entities ON glpi_tickets.entities_id = glpi_entities.id
            WHERE 
                  glpi_tickets.is_deleted = '0'";

      $result_ano = $DB->query($sql_ano);
      $total_ano = $DB->fetch_assoc($result_ano);
            
      // Número de chamados no mês atual.
      $sql_mes =	
            "SELECT
                  COUNT(glpi_tickets.id) AS total
            FROM 
                  glpi_tickets
                  LEFT JOIN glpi_entities ON glpi_tickets.entities_id = glpi_entities.id
            WHERE 
                  glpi_tickets.date LIKE '$month%'      
                  AND glpi_tickets.is_deleted = '0'";

      $result_mes = $DB->query($sql_mes);
      $total_mes = $DB->fetch_assoc($result_mes);

      //Número de chamados no dia atual.
      $sql_hoje =	
            "SELECT 
                  COUNT(glpi_tickets.id) as total        
            FROM 
                  glpi_tickets
                  LEFT JOIN glpi_entities ON glpi_tickets.entities_id = glpi_entities.id
            WHERE 
                  glpi_tickets.date like '$hoje%'      
                  AND glpi_tickets.is_deleted = '0'";

      $result_hoje = $DB->query($sql_hoje);
      $total_hoje = $DB->fetch_assoc($result_hoje);

      // Total de usuários.
      $sql_users = 
            "SELECT COUNT(id) AS total
            FROM 
                  glpi_users
            WHERE 
                  is_deleted = 0
                  AND is_active = 1";

      $result_users = $DB->query($sql_users);
      $total_users = $DB->fetch_assoc($result_users);

?>