<link href="css/diagrama_fluxo_cumulativo.css" rel="stylesheet" type="text/css" />

<?php
  // Recupera chamados mensais
  $query_chamados_mensais = 
      "SELECT DISTINCT
        DATE_FORMAT(date, '%b-%y') as month_l,
        COUNT(id) as nb, 
        DATE_FORMAT(date, '%y-%m') as month
      FROM glpi_tickets
      WHERE glpi_tickets.is_deleted = '0'
      GROUP BY month
      ORDER BY month";
      
  $result_chamados_mensais = $DB->query($query_chamados_mensais) or die('ERRO');
  $arr_grfm = array();
  while ($row_result = $DB->fetch_assoc($result_chamados_mensais)) { 
    $v_row_result = $row_result['month_l'];
    $arr_grfm[$v_row_result] = $row_result['nb'];			
  }
  
  $grfm = array_keys($arr_grfm) ;
  $quantm = array_values($arr_grfm) ;
    
  $grfm2 = implode("','",$grfm);
  $grfm3 = "'$grfm2'";
  $quantm2 = implode(',',$quantm);
    
  //array to compare months
  $DB->data_seek($result_chamados_mensais, 0);
    
  $arr_month = array();
  while ($row_result = $DB->fetch_assoc($result_chamados_mensais)) { 
    $v_row_result = $row_result['month_l'];
    $arr_month[$v_row_result] = 0;			
  } 

  //chamados abertos mensais
  $status = "('2','1','3','4')";
    
  $querya = 
      "SELECT DISTINCT
        DATE_FORMAT(date, '%b-%y') as month_l,
        COUNT(id) as nb,
        DATE_FORMAT(date, '%y-%m') as month
      FROM glpi_tickets
      WHERE 
        glpi_tickets.is_deleted = '0'
        AND glpi_tickets.status IN ". $status ."
      GROUP BY month
      ORDER BY month";
          
  $resulta = $DB->query($querya) or die('erro');

  $arr_grfa = array();
  while ($row_result = $DB->fetch_assoc($resulta)) { 
    $v_row_result = $row_result['month_l'];
    $arr_grfa[$v_row_result] = $row_result['nb'];			
  } 
    
  $arr_open = array_merge($arr_month, $arr_grfa);
     
  $grfa = array_keys($arr_open) ;
  $quanta = array_values($arr_open) ;
    
  $grfa2 = implode("','",$grfa);
  $grfa3 = "'$grfa2'";
  $quanta2 = implode(',',$quanta);
    
  // fechados mensais

  $queryf = 
      "SELECT DISTINCT
        DATE_FORMAT(date, '%b-%y') as month_l,
        COUNT(id) as nb,
        DATE_FORMAT(date, '%y-%m') as month
      FROM glpi_tickets
      WHERE 
        glpi_tickets.is_deleted = '0'
        AND glpi_tickets.status NOT IN ". $status ."
      GROUP BY month
      ORDER BY month";
        
  $resultf = $DB->query($queryf) or die('erro');
      
  $arr_grff = array();
  while ($row_result = $DB->fetch_assoc($resultf)) { 
    $v_row_result = $row_result['month_l'];
    $arr_grff[$v_row_result] = $row_result['nb'];			
  } 
    	  
  $grff = array_keys($arr_grff) ;
  $quantf = array_values($arr_grff) ;
    
  $grff2 = implode("','",$grff);
  $grff3 = "'$grff2'";
  $quantf2 = implode(',',$quantf);
  
?>

<script type='text/javascript'>
  $(function () {		
    $('#cfd-container').highcharts({
      chart: {
        type: 'areaspline',
        backgroundColor: 'transparent'
      },
      title: {
        text: 'Diagrama de Fluxo Cumulativo'
      },
      legend: {
        layout: 'horizontal',
        align: 'center',
        verticalAlign: 'bottom',
        x: 0,
        y: 0,
        loating: true,
        adjustChartSize: true
      },
      xAxis: {
        categories: [<?php echo $grfm3; ?>],
        labels: {
          rotation: -55,
          align: 'right',
          style: {
            fontSize: '11px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },	
      yAxis: {
        minPadding: 0,
        maxPadding: 0,
        min: 0, 
        showLastLabel:false,  
        title: {
          text: '<?php echo  __('Tickets','dashboard'); ?>'
        }
      }, 
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 2,
          borderColor: 'white',
          shadow:true,           
          showInLegend: true
        },
        areaspline: {
          fillOpacity: 0.5
        }
      },     
      tooltip: {
        shared: true
      },
      credits: {
        enabled: false
      },                  
      series: [{
        name: '<?php echo __('Opened','dashboard'); ?>', 
        dataLabels: {
          enabled: true,                    
          color: '#000000',
          style: {
            fontSize: '11px',
            fontFamily: 'Verdana, sans-serif',
            fontWeight: 'bold'
          },
        },               
        data: [<?php echo $quantm2; ?>] 
      }, {
        name: '<?php echo __('Closed','dashboard'); ?>',                            
        data: [<?php echo $quantf2; ?>]
      }, {
        name: '<?php echo __('Late','dashboard'); ?>',
        dataLabels: {
          enabled: true,
          color: '#800000',
          style: {
            fontSize: '11px',
            fontFamily: 'Verdana, sans-serif',
            fontWeight: 'bold'
          },
        },   
        data: [<?php echo $quanta2; ?>] 
      }]
    });
  });
    
</script>
