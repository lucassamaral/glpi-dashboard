<?php

  // Recupera a quantidade de chamados não solucionados por entidade.
  $query_chamados =
      "SELECT 
        entitie.name AS entidade, 
        COUNT( ticket.id ) AS quantidade
      FROM
        glpi_tickets AS ticket
        INNER JOIN glpi_entities AS entitie ON entitie.id = ticket.entities_id
      WHERE 
        ticket.solvedate IS NULL";
        
  $result_chamados = $DB->query($query_chamados);
  
  $array_entidades = array();
  $array_quantidade = array();
  $count = 0;
  
  while ($row_chamados = $DB->fetch_assoc($result_chamados)) {
    $array_entidades[$count] = $row_chamados['entidade'];
    $array_quantidade[$count] = $row_chamados['quantidade'];
    $count = $count + 1;
  }
  
  $contagem = count($array_entidades);
    
  $series = '[';
  for($i = 0; $i < $contagem; $i++) {    
    $series .= '{name: \'' . $array_entidades[$i] . '\', y: '.$array_quantidade[$i].'}, ';
  } 
  $series .= ']';

?>

<script type='text/javascript'>
  $(function () {                     
    $('#grafico-chamados-nao-solucionados-container').highcharts({
      chart: {
        type: 'pie',
        backgroundColor: 'transparent',
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        margin: [0, 0, 0, 0],
        spacingTop: 0,
        spacingBottom: 0,
        spacingLeft: 0,
        spacingRight: 0
      },
      title: {
        text: '<?php echo __('Chamados Não Solucionados por Entidade','dashboard') ?>'
      },
      legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'middle',
        x: 0,
        y: 0,
        borderWidth: 0,
        backgroundColor: '#FFFFFF',
        adjustChartSize: false,
        format: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          size: '50%',
          innerSize: 90,
          depth: 40,
          dataLabels: {
            format: '{point.y}',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
          },
          showInLegend: true
        }
      },
      series: [{
        type: 'pie',
        name: '<?php echo __('Tickets','dashboard') ?>',
        data: <?php echo $series ?>
      }]
    });
  });
</script>