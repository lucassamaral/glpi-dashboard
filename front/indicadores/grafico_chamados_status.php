<?php
  // Recupera a quantidade de chamados agrupados por status.
  $query_chamados_status = 
      "SELECT 
        COUNT(ticket.id) AS quantidade, 
        ticket.status AS status
      FROM 
        glpi_tickets AS ticket
      WHERE 
        ticket.is_deleted = '0'
        AND ticket.status != 6        
      GROUP BY ticket.status
      ORDER BY ticket.status ASC";
        
  $result_chamados_status = $DB->query($query_chamados_status);
    
  $array_grafico = array();
  $quantidade_status = array();
  $count = 0;
  while ($row_chamados_status = $DB->fetch_assoc($result_chamados_status)) { 
    $array_grafico[$count] = $row_chamados_status['status'];
    $quantidade_status[$count] = $row_chamados_status['quantidade'];
    $count = $count + 1;			
	}
    
  $grafico_chamados_status = array_keys($array_grafico);
  $contagem = count($array_grafico);
    
  $series = '[';
  for($i = 0; $i < $contagem; $i++) {    
    $series .= '{name: \'' . Ticket::getStatus($array_grafico[$i]) . '\', y: '.$quantidade_status[$i].'}, ';
  } 
  $series .= ']';
  
?>

<script type='text/javascript'>
  $(function () {                     
    $('#grafico-chamados-status-container').highcharts({
      chart: {
        type: 'pie',
        renderTo: 'container',
        backgroundColor: 'transparent',
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        margin: [0, 0, 0, 0],
        spacingTop: 0,
        spacingBottom: 0,
        spacingLeft: 0,
        spacingRight: 0

      },
      title: {
        text: '<?php echo __('Tickets by Status','dashboard') ?>'
      },
      legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'middle',
        x: 0,
        y: 0,
        borderWidth: 0,
        backgroundColor: '#FFFFFF',
        adjustChartSize: false,
        format: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          size: '50%',
          innerSize: 90,
          depth: 40,
          dataLabels: {
            format: '{point.y}',
            style: {
              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
            }
          },
          showInLegend: true
        }
      },
      series: [{
        type: 'pie',
        name: '<?php echo __('Tickets','dashboard') ?>',
        data: <?php echo $series ?>
      }]
    });
  });
</script>