<?php
  $mounth_array = array();
  $lead_array = array();
  $cycle_array = array();
    
  $last_lead_time;
  $last_cycle_time;
    
  $lead_result;
  $count = 0;
  while ($row = $DB->fetch_assoc($result_lead_time)) {
    $lead_result[$count] = $row;
    $count = $count + 1;
  }
    
  $lead_result_size = count($lead_result);
  $result_lead_time = $DB->query($query_lead_time);
  $lead_count = 0;
  while($row_lead_time = $DB->fetch_assoc($result_lead_time)) {
    if ($lead_count < 6) {
      if ($lead_count + 1 < $lead_result_size) {
        $last_lead_time = $lead_result[$lead_count + 1]['lead_time'];
      }
      
      $cycle_time;
      $cycle_result;
      $count = 0;
      while ($row = $DB->fetch_assoc($result_cycle_time)) {
        $cycle_result[$count] = $row;
        $count = $count + 1;
      }
      $cycle_result_size = count($cycle_result);
      $result_cycle_time = $DB->query($query_cycle_time);
      $cycle_count = 0;
      while($row_cycle_time = $DB->fetch_assoc($result_cycle_time)) {
        if ($cycle_count < 6) {
          if ($cycle_count  + 1 < $cycle_result_size) {
            $last_cycle_time = $cycle_result[$cycle_count + 1]['cycle_time'];
          }
          
          if ($row_lead_time['month'] === $row_cycle_time['month']) {
            $cycle_time = $row_cycle_time['cycle_time'];
            break;
          }
        }
      }
        
      array_push($mounth_array, transformMonth($row_lead_time['month']) . "/" . $row_lead_time['year']);
      array_push($lead_array, floatval($row_lead_time['lead_time']));
      array_push($cycle_array, floatval($cycle_time));
        
      $lead_count ++;
      $cycle_count ++;
    }
  }      
?>

<script type="text/javascript">
  $(function () {
    $('#grafico-cycle-lead-time-container').highcharts({
      chart: {
        type: 'column',
        backgroundColor: 'transparent'
      },
      title: {
        text: 'Gráfico de Variação do Lead e Cycle Time'
      },
      xAxis: {
        categories: <?php echo json_encode(array_reverse($mounth_array)); ?>
      },
      yAxis: {
        allowDecimals: false,
        title: {
          text: 'Tempo médio (minutos)'
        },
        labels: {
          formatter: function () {
            return this.value;
          }
        }
      },
      tooltip: {
        formatter: function() {
          return transformTime(this.y);
        }
      },
      series: [{
        name: 'Lead Time',
        data: <?php echo json_encode(array_reverse($lead_array)); ?>, 
        color: '#309E52'
      }, {
        name: 'Cycle Time',
        data: <?php echo json_encode(array_reverse($cycle_array)); ?>,
        color: '#6637FF'
      }]
    });
  });
    
  function transformTime(time) {
    var days = Math.floor(time/1440);
    var hours = Math.floor(Math.floor(time%1440)/60);
    var minutes = Math.floor(time%1440) - Math.floor(Math.floor(time%1440)/60) * 60;
    var dateString = "";
      
    if (days > 0) {
      dateString += (days + (days == 1 ? " dia" : " dias"));
      if (hours > 0 && minutes > 0) {
        dateString += ", ";
      } else {
        if (hours > 0 || minutes > 0) {
          dateString += " e ";
        }
      }
    }
      
    if (hours > 0) {
      dateString += (hours + (hours == 1 ? " hora" : " horas"));
      if (minutes > 0) {
        dateString += " e ";
      }
    }
     
    if (minutes > 0) {
      dateString += (minutes + (minutes == 1 ? " minuto" : " minutos"));
    }
    
    return dateString;
  }

</script>
