<?php
	define('GLPI_ROOT', '../../../../');
	include (GLPI_ROOT . "/inc/includes.php");
	include (GLPI_ROOT . "/config/config.php");
    
	global $DB;
    
//	Session::checkLoginUser();
//	Session::checkRight("profile", "r");
    
  switch (date("m")) {  
    case "01": $mes = __('January','dashboard'); break;
    case "02": $mes = __('February','dashboard'); break;
    case "03": $mes = __('March','dashboard'); break;
    case "04": $mes = __('April','dashboard'); break;
    case "05": $mes = __('May','dashboard'); break;
    case "06": $mes = __('June','dashboard'); break;
    case "07": $mes = __('July','dashboard'); break;
    case "08": $mes = __('August','dashboard'); break;
    case "09": $mes = __('September','dashboard'); break;
    case "10": $mes = __('October','dashboard'); break;
    case "11": $mes = __('November','dashboard'); break;
    case "12": $mes = __('December','dashboard'); break;
  }
    
	switch (date("w")) {
    case "0": $dia = __('Sunday','dashboard'); break;    
    case "1": $dia = __('Monday','dashboard'); break;
    case "2": $dia = __('Tuesday','dashboard'); break;
    case "3": $dia = __('Wednesday','dashboard'); break;
    case "4": $dia = __('Thursday','dashboard'); break;
    case "5": $dia = __('Friday','dashboard'); break;
    case "6": $dia = __('Saturday','dashboard'); break;  
  }
    
?>

<!DOCTYPE html>
<html>
	<head>
    <title>GLPI - Dashboard - Indicadores</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="refresh" content= "120"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      
    <link rel="icon" href="../img/dash.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="../img/dash.ico" type="image/x-icon" />
      
    <!-- Bootstrap 
    <link href="css/bootstrap-responsive.css" rel="stylesheet" />
    <link href="css/bootstrap-overrides.css" type="text/css" rel="stylesheet" />    
    -->
    <link href="../css/bootstrap3.css" rel="stylesheet">
      
    <!-- Styles -->
    <link href="../css/styled.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/layout.css">
    <link rel="stylesheet" type="text/css" href="../css/elements.css">
    <link rel="stylesheet" type="text/css" href="../css/icons.css">
    <!-- Custom Styles -->
    <link href="../css/custom.css" rel="stylesheet">
      
    <!-- this page specific styles -->
    <link rel="stylesheet" href="../css/compiled/index.css" type="text/css" media="screen" />    
    
    <!-- open sans font -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <!-- lato font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <link href="../css/style-dash.css" rel="stylesheet" type="text/css" />
    <link href="../css/dashboard.css" rel="stylesheet" type="text/css" />
		<link href="css/indicadores.css" rel="stylesheet" type="text/css" />
		<link href="css/carousel.css" rel="stylesheet" type="text/css" />
      
    <link href="../less/style.less" rel="stylesheet"  title="lessCss" id="lessCss">
      
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="../js/jquery.js"></script>
      
		<script type="text/javascript">
			$(function($) {
				var options = {
					timeNotation: '24h',
					am_pm: false
				};
				$('#clock').jclock(options);
			});
		</script> 
	</head>
    
	<body>
    <div class="site-holder">
      <div class="container-fluid">
        <ul class="slider">
          <li>
            <div class="row">          
              <?php
                include("dados_odometro.php");
              ?>
              <div class="col-sm-6 col-md-6">
                <!-- COLUMN 1 -->
                <div class="row">
                  <div class="col-sm-12 col-md-12">	
                    <div class="dashbox panel panel-default" style="padding: 10px; text-align: center">
                      <span class="chamado">		
                        <script type="text/javascript">
                          var d_names = <?php echo '"'.$dia.'"' ; ?>;
                          var m_names = <?php echo '"'.$mes.'"' ; ?>;
                            						        
                          var d = new Date();
                          var curr_day = d.getDay();
                          var curr_date = d.getDate();
                          var curr_month = d.getMonth();
                          var curr_year = d.getFullYear();
                            					      
                          document.write(d_names + ", " + curr_date + " " + m_names + " " + curr_year + " " );		
                        </script> 
                      </span>
                      <span id="clock" class="chamado"></span>
                    </div>
                  </div>
                </div>
                <div class="row">	
                  <div class="col-sm-6 col-md-6 stat">
                    <div class="dashbox panel panel-default">
                      <div class="panel-body">
                        <div class="panel-left red">
                          <i class="fa fa-calendar-o fa-3x" style="color:#D9534F;"></i>
                        </div>
                        <div class="panel-right">
                          <div id="odometer1" class="odometer" style="color: #32a0ee; font-size: 25px;"></div><p></p>
                          <span class="chamado"><?php echo __('Tickets','dashboard'); ?></span><br>
                          <span class="date"><b><?php echo __('Today','dashboard'); ?></b></span>
                        </div>
                      </div>
                    </div>
                  </div>						  
                  <div class="col-sm-6 col-md-6">
                    <div class="dashbox panel panel-default">
                      <div class="panel-body">
                        <div class="panel-left blue">
                          <i class="fa fa-calendar fa-3x" style="color:#298EE3;"></i>
                        </div>
                        <div class="panel-right">										 
                          <div id="odometer2" class="odometer" style="color: #32a0ee; font-size: 25px;"></div><p></p>
                          <span class="chamado"><?php echo __('Tickets','dashboard'); ?></span><br>
                          <span class="date"><b><?php echo $mes ?></b></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <div class="dashbox panel panel-default">
                      <div class="panel-body">
                        <div class="panel-left red">
                          <i class="fa fa-plus-square fa-3x" style="color:#F1B119;"></i>
                        </div>
                        <div class="panel-right">
                          <div id="odometer3" class="odometer" style="color: #32a0ee; font-size: 25px;"></div><p></p>
                          <span class="chamado"><?php echo __('Tickets','dashboard'); ?></span><br>
                          <span class="date"><b><?php echo __('Total','dashboard'); ?></b></span>
                        </div>										   
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <div class="dashbox panel panel-default">
                      <div class="panel-body">
                        <div class="panel-left blue">
                          <i class="fa fa-users fa-3x" style="color:#008F3A;"></i>
                        </div>
                        <div class="panel-right">
                          <div id="odometer4" class="odometer" style="color: #32a0ee; font-size: 25px;"></div><p></p>
                          <span class="chamado"><?php echo __('users','dashboard'); ?></span><br>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                
              <script type="text/javascript" >
                window.odometerOptions = {
                  format: '( ddd).dd'
                };
                  
                setTimeout(function(){
                  odometer1.innerHTML = <?php echo $total_hoje['total']; ?>;
                  odometer2.innerHTML = <?php echo $total_mes['total']; ?>;
                  odometer3.innerHTML = <?php echo $total_ano['total']; ?>;
                  odometer4.innerHTML = <?php echo $total_users['total']; ?>;
                }, 1000);
              </script>
                
              <?php 
                include("util/cycle_lead_time_util.php");
                include("util/funcionarios_stats_util.php");
              ?>
              
              <div class="col-sm-6 col-md-6">
                <?php 
                  include("tabela_cycle_lead_time.php");
                ?>
              </div>
            </div>
    
            <div class="row">
              <div class="col-sm-6 col-md-6" id="cfd-container">
                <?php
                  include("diagrama_fluxo_cumulativo.php");
                ?>
              </div>
              <div class="col-sm-6 col-md-6" id="grafico-cycle-lead-time-container">
                <?php
                  include("grafico_cycle_lead_time.php");
                ?>
              </div> 
            </div>
          </li>
          <li>
            <div class="row">
              <div class="col-sm-6 col-md-6" id="grafico-chamados-status-container">
                <?php
                  include("grafico_chamados_status.php");
                ?>
              </div>
              <div class="col-sm-6 col-md-6" id="grafico-chamados-nao-solucionados-container">
                <?php
                  include("grafico_chamados_nao_solucionados.php");
                ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-md-4">
                <?php 
                  include("tabela_chamados_solucionados.php");
                ?>
              </div>
              <div class="col-sm-4 col-md-4">
                <?php 
                  include("tabela_chamados_nao_solucionados.php");
                ?>
              </div>
              <div class="col-sm-4 col-md-4">
                <?php 
                  include("tabela_cycle_time_individual.php");
                ?>
              </div>
            </div>
          </li>
        </ul>
      </div>
       
      <script type="text/javascript">
        var slider = $('.slider');
        var slide = 'li'; 
        var transition_time = 1000; 
        var time_between_slides = 20000; 
          
        function slides(){
          return slider.find(slide);
        }
          
        slides().fadeOut();
          
        // set active classes
        slides().first().addClass('active');
        slides().first().fadeIn(transition_time);
          
        // auto scroll 
        $interval = setInterval(
          function() {
            var i = slider.find(slide + '.active').index();
              
            slides().eq(i).removeClass('active');
            slides().eq(i).fadeOut(transition_time);
              
            if (slides().length == i + 1) i = -1; // loop to start
              
            slides().eq(i + 1).fadeIn(transition_time);
            slides().eq(i + 1).addClass('active');
          }, transition_time + time_between_slides 
        );
      </script>
    </div>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="../js/less-1.5.0.min.js"></script>
    <script src="../js/jquery.storage.js"></script>        
    <script src="../js/jquery.accordion.js"></script>
    <script src="../js/bootstrap-typeahead.js"></script>                
    <script src="../js/bootstrap-progressbar.js"></script>
    <script src="../js/galaxy/hovermenu.js" charset="utf-8"></script>
    <script src="../js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="../js/jquery.easy-pie-chart.js"></script>
    <script src="../js/bootstrap-switch.js"></script>
    <script src="../js/jquery.address-1.6.min.js"></script>
    <script src="../js/highcharts.js" type="text/javascript" ></script>
    <script src="../js/highcharts-3d.js" type="text/javascript" ></script>
    <script src="../js/modules/exporting.js" type="text/javascript" ></script>
    <script src="../js/themes/grid.js" type="text/javascript" ></script>
    <!-- knob -- >
    <script src="../js/jquery.knob.js"></script>
    <!-- flot charts -->    
    <script src="../js/jquery.flot.js"></script>
    <script src="../js/jquery.flot.stack.js"></script>
    <script src="../js/jquery.flot.resize.js"></script>
    <script src="../js/jquery.flot.pie.min.js"></script>
    <script src="../js/jquery.flot.valuelabels.js"></script>
    <script src="../js/theme.js"></script>         
    <script src="../js/jquery.jclock.js"></script>
    <!-- odometer -->
    <link href="../css/odometer.css" rel="stylesheet">
    <script src="../js/odometer.js"></script>
  </body>
 </html>
