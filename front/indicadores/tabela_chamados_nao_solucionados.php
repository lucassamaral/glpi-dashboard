<link href="css/tabela_chamados_solucionados.css" rel="stylesheet" type="text/css" />

<div class="widget widget-table action-table striped table-cycle-lead-time">
	<div class="widget-header"> <i class="fa fa-list-alt" style="margin-left:7px;"></i>
		<h3>
      <a href="../../../front/ticket.php" target="_blank" style="color: #525252;">
        <?php echo __('Número de Chamados Não Solucionados','dashboard'); ?>
      </a>
    </h3>
  </div>
    
  <div class="widget-content">
    <table id="last_tickets" class="table table-hover table-bordered table-condensed" >
      <th style="text-align: center;">Funcionário</th>
      <th style="text-align: center;">Chamados</th>
        
      <?php
        while ($row = $DB->fetch_assoc($result_funcionario_nao_solucionados)) { 
          echo
            "<tr>
		<td style='text-align: center;'>" . $row['user'] . "</td>
		<td style='text-align: center; position:relative'>
                  <div>" . $row['count'] . "</div>
		</td>
	     </tr>";
        }
      ?>
      
    </table>
  </div>
</div>
