<link href="css/tabela_cycle_lead_time.css" rel="stylesheet" type="text/css" />

<div class="widget widget-table action-table striped table-cycle-lead-time">
	<div class="widget-header"> <i class="fa fa-list-alt" style="margin-left:7px;"></i>
		<h3><a href="../../../front/ticket.php" target="_blank" style="color: #525252;"><?php echo __('Tabela de Variação do Lead e Cycle Time','dashboard'); ?></a></h3>
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
		<table id="last_tickets" class="table table-hover table-bordered table-condensed" >
			<th style="text-align: center;">Mês</th>
			<th style="text-align: center;">Lead Time</th>
			<th style="text-align: center;">Cycle Time</th>

			<?php
				$last_lead_time;
				$last_cycle_time;

				$lead_result;
				$count = 0;
				while ($row = $DB->fetch_assoc($result_lead_time)) {
					$lead_result[$count] = $row;
					$count = $count + 1;
				}
				$lead_result_size = count($lead_result);
				$result_lead_time = $DB->query($query_lead_time);
				$lead_count = 0;
				while($row_lead_time = $DB->fetch_assoc($result_lead_time)) {
					if ($lead_count < 6) {
						if ($lead_count + 1 < $lead_result_size) {
							$last_lead_time = $lead_result[$lead_count + 1]['lead_time'];
						}

						$cycle_time;
						$cycle_result;
						$count = 0;
						while ($row = $DB->fetch_assoc($result_cycle_time)) {
							$cycle_result[$count] = $row;
							$count = $count + 1;
						}
						$cycle_result_size = count($cycle_result);
						$result_cycle_time = $DB->query($query_cycle_time);
						$cycle_count = 0;
						while($row_cycle_time = $DB->fetch_assoc($result_cycle_time)) 
						{
							if ($cycle_count < 6) {
								if ($cycle_count  + 1 < $cycle_result_size) {
									$last_cycle_time = $cycle_result[$cycle_count + 1]['cycle_time'];
								}

								if ($row_lead_time['month'] === $row_cycle_time['month']) 
								{
									$cycle_time = $row_cycle_time['cycle_time'];
									break;
								}
							}
						}

						if ($row_lead_time['lead_time'] < $last_lead_time) {
							$lead_style = "arrow_down";
						} elseif ($row_lead_time['lead_time'] > $last_lead_time) {
							$lead_style = "arrow_up";
						} else {
							$lead_style = "stable";
						}

						if ($cycle_time < $last_cycle_time) {
							$cycle_style = "arrow_down";
						} elseif ($cycle_time > $last_cycle_time) {
							$cycle_style = "arrow_up";
						} else {
							$cycle_style = "stable";
						}

						echo 
							"<tr>"
								."<td style='text-align: center;'>" . transformMonth($row_lead_time['month']) . "/" . $row_lead_time['year'] . "</td>"
								."<td style='text-align: center; position:relative'>
									<div class='arrow'>
										<img src='icones/" . $lead_style . ".png' />
									</div>
									<div>" . transformTime($row_lead_time['lead_time']) . "</div>
								</td>"
								."<td style='text-align: center;'>
									<div class='arrow'>
										<img src='icones/" . $cycle_style . ".png'/>
									</div>
									<div>" . transformTime($cycle_time) . "</div>
								</td>"
							."</tr>";

						$lead_count ++;
						$cycle_count ++;
					}
				}				
			?>                                       
		</table>
	</div>
</div>
