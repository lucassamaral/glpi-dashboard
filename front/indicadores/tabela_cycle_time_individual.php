<link href="css/tabela_chamados_solucionados.css" rel="stylesheet" type="text/css" />

<div class="widget widget-table action-table striped table-cycle-lead-time">
	<div class="widget-header"> <i class="fa fa-list-alt" style="margin-left:7px;"></i>
		<h3>
      <a href="../../../front/ticket.php" target="_blank" style="color: #525252;">
        <?php echo __('Cycle Time Individual','dashboard'); ?>
      </a>
    </h3>
  </div>
    
  <div class="widget-content">
    <table id="last_tickets" class="table table-hover table-bordered table-condensed" >
      <th style="text-align: center;">Funcionário</th>
      <th style="text-align: center;">Chamados</th>
        
      <?php
        $i = 0;
	while ($row = $DB->fetch_assoc($result_cycle_time_individual)) {
          $icone_medalha = "";
          if ($i == 0) {
            $icone_medalha = "<img src='icones/medalha_ouro.png' />";
          } elseif ($i == 1) {
            $icone_medalha = "<img src='icones/medalha_prata.png' />";
          } elseif ($i == 2) {
            $icone_medalha = "<img src='icones/medalha_bronze.png' />";
          }
          
          echo
            "<tr>
	       <td style='text-align: center;'>" . $row['user'] . "</td>
	       <td style='text-align: center; position:relative'>
                 <div class='medal'>". $icone_medalha ."</div>
                 <div>" . transformTime($row['cycle_time']) . "</div>
	       </td>
	     </tr>";

	  $i++;
        }
      ?>
      
    </table>
  </div>
</div>
