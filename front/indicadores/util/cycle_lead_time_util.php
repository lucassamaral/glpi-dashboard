<?php	
	
	// Recupera o Lead Time médio por mês.
	// Corresponde à média dos valores da diferença entre a data de abertura do chamado (coluna 'date')
	// e sua data de fechamento (coluna 'closedate').
	$query_lead_time =
		"SELECT 
			AVG(TIMESTAMPDIFF(MINUTE, ticket.date, ticket.closedate)) AS 'lead_time',
			DATE_FORMAT(ticket.closedate, '%m') AS 'month',
			DATE_FORMAT(ticket.closedate, '%Y') AS 'year'
		FROM 
			glpi_tickets AS ticket
		WHERE 
			ticket.is_deleted = '0'
			AND ticket.closedate IS NOT NULL
			AND ticket.closedate >= DATE_SUB(NOW(), INTERVAL 7 MONTH)
		GROUP BY DATE_FORMAT(ticket.closedate, '%m')
		ORDER BY ticket.closedate DESC";

	$result_lead_time = $DB->query($query_lead_time);

	// Recupera o Cycle Time médio por mês.
	// Corresponde à média dos valores da diferença entre a data em que a situação do chamado foi alterada para
	// Processando (atribuído ou planejado) ou Pendente (obtidas da tabela 'glpi_logs') e sua data de resolução (coluna 'solvedate').
	// e sua data de fechamento (coluna 'closedate').
	$query_cycle_time = 
		"SELECT 
			AVG(TIMESTAMPDIFF(MINUTE, ticket_log.date_mod, ticket.solvedate)) AS 'cycle_time',
			DATE_FORMAT(ticket.solvedate, '%m') AS 'month',
			DATE_FORMAT(ticket.solvedate, '%Y') AS 'year'
		FROM 
			glpi_tickets AS ticket
			INNER JOIN 
				glpi_logs AS ticket_log ON ticket.id = ticket_log.items_id
		WHERE 
			ticket.is_deleted = '0'
			AND ticket.solvedate IS NOT NULL
			AND ticket.solvedate >= DATE_SUB(NOW(), INTERVAL 7 MONTH)
			AND ticket_log.itemtype = 'Ticket'
			AND ticket_log.id_search_option = 12
			AND ticket_log.new_value IN (2, 3, 4)
		GROUP BY DATE_FORMAT(ticket.solvedate, '%m')
		ORDER BY ticket.solvedate DESC";

	$result_cycle_time = $DB->query($query_cycle_time);
?>

<?php

	/*
	 * Métodos.
	 */

	function transformTime($time) {
		$days = floor($time/1440);
		$hours = floor(floor($time%1440)/60);
		$minutes =  floor($time%1440) - floor(floor($time%1440)/60) * 60;


		$time = "";

		if ($days > 0) {
			$time .= $days . " dias";
		}

		if ($hours > 0) {
			if ($days > 0 && $minutes > 0) {
				$time .= ", " . $hours;
			} else if ($days > 0 && $minutes == 0) {
				$time .= " e " . $minutes;
			}
		}

		if ($minutes > 0) {
			
		}
		
		$dateString = "";

		if ($days > 0) {
			$dateString .= $days . " dias";
			if ($hours > 0 && $minutes > 0) {
				$dateString .= ", ";
			} else {
				if ($hours > 0 || $minutes > 0) {
					$dateString .= " e ";
				}
			}
		}

		if ($hours > 0) {
			$dateString .= $hours . " horas";
			if ($minutes > 0) {
				$dateString .= " e ";
			}
		}

		if ($minutes > 0) {
			$dateString .= $minutes . " minutos";
		}

		return $dateString;
	}

	function transformMonth($month) {
		switch ($month) {
			case 1:
				return "Janeiro";
				break;
			case 2:
				return "Fevereiro";
				break;
			case 3:
				return "Março";
				break;
			case 4:
				return "Abril";
				break;
			case 5:
				return "Maio";
				break;
			case 6:
				return "Junho";
				break;
			case 7:
				return "Julho";
				break;
			case 8:
				return "Agosto";
				break;
			case 9:
				return "Setembro";
				break;
			case 10:
				return "Outubro";
				break;
			case 11:
				return "Novembro";
				break;
			case 12:
				return "Dezembro";
				break;			
		}
	}

?>
