<?php

	// Recupera a lista de funcionários e a quantidade de tickets solucionados no último mês.
	$query_funcionario_solucionados =
		"SELECT
			user.firstname AS 'user',
			COUNT(ticket.id) AS 'count'
		FROM
			glpi_tickets AS ticket
			INNER JOIN glpi_users AS user ON ticket.users_id_lastupdater = user.id
		WHERE
			ticket.is_deleted = '0'
			AND ticket.solvedate IS NOT NULL
			AND user.firstname IS NOT NULL
			AND ticket.solvedate >= DATE_SUB(NOW(), INTERVAL 4 MONTH)
		GROUP BY user.firstname
		ORDER BY COUNT(ticket.id) DESC LIMIT 6";

	$result_funcionario_solucionados = $DB->query($query_funcionario_solucionados);


	// Recupera a lista de funcionários e a quantidade de tickets não solucionados no último mês.
	$query_funcionario_nao_solucionados =
		"SELECT
			user.firstname AS 'user',
			COUNT(ticket.id) AS 'count'
		FROM
			glpi_tickets AS ticket
			INNER JOIN glpi_users AS user ON ticket.users_id_lastupdater = user.id
		WHERE
			ticket.is_deleted = '0'
			AND ticket.solvedate IS NULL
			AND user.firstname IS NOT NULL
		GROUP BY user.firstname
		ORDER BY COUNT(ticket.id) DESC LIMIT 6";

	$result_funcionario_nao_solucionados = $DB->query($query_funcionario_nao_solucionados);

	// Retorna a lista de funcionários e seu respectivo cycle time individual.
	$query_cycle_time_individual =
		"SELECT 
			user.firstname AS 'user',
			AVG(TIMESTAMPDIFF(MINUTE, ticket_log.date_mod, ticket.solvedate)) AS 'cycle_time'
		FROM 
			glpi_tickets AS ticket
			INNER JOIN glpi_logs AS ticket_log ON ticket.id = ticket_log.items_id
			INNER JOIN glpi_users AS user ON ticket.users_id_lastupdater = user.id
		WHERE 
			ticket.is_deleted = '0'
			AND ticket.solvedate IS NOT NULL
			AND ticket_log.itemtype = 'Ticket'
			AND ticket_log.id_search_option = 12
			AND ticket_log.new_value IN (2, 3, 4)
			AND user.firstname IS NOT NULL
		GROUP BY user.firstname
		ORDER BY AVG(TIMESTAMPDIFF(MINUTE, ticket_log.date_mod, ticket.solvedate)) ASC LIMIT 6";

	$result_cycle_time_individual = $DB->query($query_cycle_time_individual);
?>
